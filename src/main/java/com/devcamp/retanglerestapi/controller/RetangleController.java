package com.devcamp.retanglerestapi.controller;

import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import com.devcamp.retanglerestapi.retangle;

@CrossOrigin
@RequestMapping("/")
@RestController
public class RetangleController {
    @GetMapping("/rectangle-area")
    public double getArea(@RequestParam(required = true, name = "width") float getWidth,
            @RequestParam(required = true, name = "height") float getHeight) {
        retangle hcm01 = new retangle(getHeight, getWidth);

        return hcm01.getArea();

    }

    @GetMapping(value = "/retangle-perimeter")
    public double getPerimeter(@RequestParam(required = true, name = "width") float getWidth,
            @RequestParam(required = true, name = "height") float getHeight) {
        retangle hcm02 = new retangle(getHeight, getWidth);

        return hcm02.getPerimeter();
    }

}
