package com.devcamp.retanglerestapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class RetangleRestApiApplication {

	public static void main(String[] args) {
		SpringApplication.run(RetangleRestApiApplication.class, args);
	}

}
